from setuptools import setup, find_packages

setup(
    name='madxtools',
    version='0.22.2',
    packages=find_packages(),
    install_requires=[
        'numpy',
        'matplotlib',
        'cpymad',
        'pandas',
        'pybt @ git+https://gitlab.cern.ch/abt-optics-and-code-repository/simulation-codes/pybt.git',
        'scipy'
    ],
)

# MAD-X tools

```bash
pip install git+https://gitlab.cern.ch/eljohnso/madx-tools.git
```

If changes are made to the madx-tools, you can get the new features by uninstalling and reinstalling:

```bash
pip uninstall madxtools -y
pip install git+https://gitlab.cern.ch/eljohnso/madx-tools.git
```


import numpy as np
from scipy.optimize import curve_fit

def gaussian_function(x, a, I, mu, sig):
    return a + I / np.sqrt(2 * np.pi * sig ** 2) * np.exp(-(x - mu) ** 2 / 2. / sig ** 2)

def do_gaussian_fit(x,y):
    mu = np.average(x, weights=np.abs(y - np.min(y)))
    sigma = np.sqrt(np.average(x**2, weights=np.abs(y - np.min(y))) - mu**2)
    p0 = [y.min(), (np.max(y) - np.min(y)) * np.sqrt(2 * np.pi * sigma**2), mu, sigma]
    popt, pcov = curve_fit(gaussian_function, x, y, p0=p0, maxfev=1000) # maxfev is the number of tries it does the fit
    return popt, pcov

def create_elliptical_mask(h, w, center=None, radius=None, a=None, b=None):

    if center is None: # use the middle of the image
        center = (int(w/2), int(h/2))
    if radius is None: # use the smallest distance between the center and image walls
        radius = min(center[0], center[1], w-center[0], h-center[1])

    Y, X = np.ogrid[:h, :w]
    dist_from_center = np.sqrt( ((X - center[0])**2)/(a**2) + ((Y-center[1])**2)/(b**2) )

    mask = dist_from_center <= radius
    return mask

def pb_ion_rigidity(E_cin_per_nucleon):
    """
    Calculate the rigidity (Brho) of a lead (Pb) ion based on its kinetic energy per nucleon.

    Parameters:
    - E_cin_per_nucleon (float): The kinetic energy per nucleon in GeV.

    Returns:
    - Brho (float): The rigidity of the Pb ion in T*m.
    
    This function calculates the rigidity (Brho) of a lead (Pb) ion using its kinetic energy per nucleon,
    taking into account the ion's properties, such as mass, charge, and energy. It uses the formula:
    
    Brho = 3.33564 * p / charge
    
    Where:
    - p is the momentum of the ion.
    - charge is the charge of the ion.

    The ion properties (A, Z, N, charge, masses) are based on the lead (Pb) ion.
    """
    # Ion properties
    A = 208.0
    Z = 82.0
    N = 126.0
    charge = 54.0
    m_proton_GeV = 0.93828
    m_neutron_GeV = 0.93957
    m_electron_GeV = 0.000511
    m_u_GeV = 0.9315
    mass_defect_GeV = Z * m_proton_GeV + N * m_neutron_GeV + (Z - charge) * m_electron_GeV - A * m_u_GeV
    E_0 = Z * m_proton_GeV + N * m_neutron_GeV - mass_defect_GeV

    p = E_0 * np.sqrt((((E_cin_per_nucleon * A) / E_0) + 1) ** 2 - 1)
    Brho = 3.33564 * p / charge # in T*m

    return(Brho)


def pb_ion_bfield(E_cin_per_nucleon):
    """
    Calculate the Bfield (G) of a lead (Pb) ion based on its kinetic energy per nucleon.

    Parameters:
    - E_cin_per_nucleon (float): The kinetic energy per nucleon in GeV.

    Returns:
    - Bfield (float): The Bfield of the Pb ion in G.
    
    This function calculates the Bfield (G) of a lead (Pb) ion using its kinetic energy per nucleon,
    taking into account the ion's properties, such as mass, charge, and energy. It uses the formula:
    
    The ion properties (A, Z, N, charge, masses) are based on the lead (Pb) ion.
    """
    # Ion properties
    A = 208.0
    Z = 82.0
    N = 126.0
    charge = 54.0
    m_proton_GeV = 0.93828
    m_neutron_GeV = 0.93957
    m_electron_GeV = 0.000511
    m_u_GeV = 0.9315
    mass_defect_GeV = Z * m_proton_GeV + N * m_neutron_GeV + (Z - charge) * m_electron_GeV - A * m_u_GeV
    E_0 = Z * m_proton_GeV + N * m_neutron_GeV - mass_defect_GeV
    p = E_0 * np.sqrt((((E_cin_per_nucleon * A) / E_0) + 1) ** 2 - 1)
    rho = 70.0789
    B = 3.3356 * p / (rho * charge) * 10000
    
    return(B)

def ekin_brho_from_bfield(bfield):
    Ekin = (193.737692/208)*(np.sqrt(((1/((3.3356/(70.0789*54)*10000)/bfield))/193.737692)**2+1)-1)
    rho = 70.0789
    Brho = rho*bfield/10000 # GeV/c
    return Ekin, Brho

def pb_ion_p_beta_gamma(E_cin_per_nucleon):
    # Ion properties
    A = 208.0
    Z = 82.0
    N = 126.0
    charge = 54.0
    m_proton_GeV = 0.93828
    m_neutron_GeV = 0.93957
    m_electron_GeV = 0.000511
    m_u_GeV = 0.9315
    mass_defect_GeV = Z * m_proton_GeV + N * m_neutron_GeV + (Z - charge) * m_electron_GeV - A * m_u_GeV
    E_0 = Z * m_proton_GeV + N * m_neutron_GeV - mass_defect_GeV

    p = E_0 * np.sqrt((((E_cin_per_nucleon * A) / E_0) + 1) ** 2 - 1)

    gamma = p/charge/0.938
    beta = np.sqrt(1-gamma**(-2))

    return p, beta, gamma

def calc_initial_condition(E_cin_per_nucleon):
    # E_cin_per_nucleon = 2.0
    # Ion properties
    A = 208.0
    Z = 82.0
    N = 126.0
    charge = 54.0
    m_proton_GeV = 0.93828
    m_neutron_GeV = 0.93957
    m_electron_GeV = 0.000511
    m_u_GeV = 0.9315
    mass_defect_GeV = Z * m_proton_GeV + N * m_neutron_GeV + (Z - charge) * m_electron_GeV - A * m_u_GeV
    E_0 = Z * m_proton_GeV + N * m_neutron_GeV - mass_defect_GeV

    p = E_0 * np.sqrt((((E_cin_per_nucleon * A) / E_0) + 1) ** 2 - 1)

    gamma = p/charge/0.938
    beta = np.sqrt(1-gamma**(-2))

    # print(p/charge)
    # print(f"gamma = {round(gamma,3)}")
    # print(f"beta = {round(beta,3)}")
    # print(f"p = {round(p/charge,3)} GeV/c")

    Brho = 3.33564*p/charge

    # Beam characteristics
    exn = 4.92e-06
    eyn = 3.4e-06
    sige = 0.000412
    ex = exn/(beta*gamma)
    ey = eyn/(beta*gamma)

    # Initial conditions
    betx0 = 53.074
    bety0 = 3.675
    alfx0 = -13.191
    alfy0 = 0.859
    Dx0 = 0.13
    Dy0 = 0.0
    Dpx0 = 0.02
    Dpy0 = 0.0
    exn = 2.53e-05
    eyn =  6.94e-06
    sige = 0.00045

    return p, exn, eyn, betx0, bety0, alfx0, alfy0, Dx0, Dy0, Dpx0, Dpy0, ex, ey, sige

def kl_divergence(n_divisions, num_part, counts_matrix):

        number_of_bins = n_divisions**2
        target_per_bin = num_part/number_of_bins # I divide the total number of particles by the number of bins
        P = np.ones((n_divisions, n_divisions))*target_per_bin # I create a square matrix with the correct number of bins
        P = P/P.sum()

        Q = counts_matrix.astype(float)/num_part # This is the matrix where I counted how many particles fall in the bins. The total number of particles inside this region is smaller than num_part.
        Q = np.where(Q==0, np.finfo(float).eps, Q) # Replace any zeros in matrix Q with the smallest positive float number to prevent division by zero errors

        D_KL = np.sum(P * np.log(P / Q)) # Calculate the KL divergence
    
        return D_KL
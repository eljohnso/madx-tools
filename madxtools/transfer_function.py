import numpy as np

def k1(current, magnet_type, Brho):
    
    A_Q74L = [0.0,
    5.000978,
    10.000304,
    50.002003,
    100.001404,
    150.001373,
    200.00238,
    250.005432,
    299.993652,
    350.001648,
    399.995728,
    449.994446,
    499.997375,
    549.999878,
    599.994873,
    650.004211,
    666.999146,
    699.991455,
    749.998169,
    799.996643]
    
    T_Q74L = [0.0,
    0.2619,
    0.492968,
    2.358847,
    4.710821,
    7.068205,
    9.426532,
    11.780794,
    14.131984,
    16.4773,
    18.814776,
    21.133094,
    23.416058,
    25.617934,
    27.638804,
    29.43015,
    29.981004,
    30.987122,
    32.347211,
    33.545971]
    
    L_Q74L = 0.74
    
    A_Q120C = [0.00,
    50.00,
    100.00,
    150.00,
    200.00,
    250.00,
    300.00,
    350.00,
    400.00,
    450.00,
    500.00,
    550.00,
    600.00,
    650.00]
    
    T_Q120C = [0.00,
    2.05,
    4.12,
    6.19,
    8.26,
    10.33,
    12.39,
    14.43,
    16.44,
    18.36,
    20.14,
    21.74,
    23.01,
    24.07]
    
    L_Q120C = 1.2
    
    A_QFL = [0,
    94.15,
    145.76,
    197.97,
    250.74,
    300.18,
    350.7,
    402,
    451.15,
    502.09,
    551.47]
    
    T_QFL = [0,
    4.86,
    7.48,
    10.158,
    12.875,
    15.377,
    17.81,
    20.045,
    21.844,
    23.217,
    24.445]
    
    L_QFL = 1.2
    
    A_QFS = [0,
    98.95,
    147.71,
    197.23,
    249.64,
    301.81,
    351.91,
    400.77,
    454.51,
    506.28,
    555.25]
    
    T_QFS = [0,
    3.462,
    5.158,
    6.875,
    8.686,
    10.48,
    12.14,
    13.57,
    14.79,
    15.75,
    16.49]
    
    L_QFS = 0.8
    
    A_Q200L = [0.0,
    5.0,
    10.0,
    50.0,
    100.0,
    150.0,
    200.0,
    250.0,
    300.0,
    350.0,
    400.0,
    450.0,
    500.0,
    540.0,
    600.0,
    650.0,
    700.0,
    750.0,
    800.0,
    850.0]
    
    T_Q200L = [0.0,
    0.20,
    0.38,
    1.85,
    3.69,
    5.54,
    7.39,
    9.23,
    11.08,
    12.91,
    14.74,
    16.54,
    18.28,
    19.58,
    21.34,
    22.57,
    23.57,
    24.38,
    25.08,
    25.67]
    
    L_Q200L = 2.0
    
    if magnet_type == "Q74L":
        measured_currents = A_Q74L
        measured_int_gradient = T_Q74L
        length = L_Q74L
        
    if magnet_type == "Q120C":
        measured_currents = A_Q120C
        measured_int_gradient = T_Q120C
        length = L_Q120C
        
    if magnet_type == "QFL":
        measured_currents = A_QFL
        measured_int_gradient = T_QFL
        length = L_QFL
        
    if magnet_type == "QFS":
        measured_currents = A_QFS
        measured_int_gradient = T_QFS
        length = L_QFS
        
    if magnet_type == "Q200L":
        measured_currents = A_Q200L
        measured_int_gradient = T_Q200L
        length = L_Q200L
        
    int_gradient = np.interp(current,measured_currents,measured_int_gradient)
    k1 = int_gradient / (length * Brho)
    
    return k1
    
def current(k, magnet_type, Brho):
    
    A_Q74L = [0.0,
    5.000978,
    10.000304,
    50.002003,
    100.001404,
    150.001373,
    200.00238,
    250.005432,
    299.993652,
    350.001648,
    399.995728,
    449.994446,
    499.997375,
    549.999878,
    599.994873,
    650.004211,
    666.999146,
    699.991455,
    749.998169,
    799.996643]
    
    T_Q74L = [0.0,
    0.2619,
    0.492968,
    2.358847,
    4.710821,
    7.068205,
    9.426532,
    11.780794,
    14.131984,
    16.4773,
    18.814776,
    21.133094,
    23.416058,
    25.617934,
    27.638804,
    29.43015,
    29.981004,
    30.987122,
    32.347211,
    33.545971]
    
    L_Q74L = 0.74
    
    A_Q120C = [0.00,
    50.00,
    100.00,
    150.00,
    200.00,
    250.00,
    300.00,
    350.00,
    400.00,
    450.00,
    500.00,
    550.00,
    600.00,
    650.00]
    
    T_Q120C = [0.00,
    2.05,
    4.12,
    6.19,
    8.26,
    10.33,
    12.39,
    14.43,
    16.44,
    18.36,
    20.14,
    21.74,
    23.01,
    24.07]
    
    L_Q120C = 1.2
    
    A_QFL = [0,
    94.15,
    145.76,
    197.97,
    250.74,
    300.18,
    350.7,
    402,
    451.15,
    502.09,
    551.47]
    
    T_QFL = [0,
    4.86,
    7.48,
    10.158,
    12.875,
    15.377,
    17.81,
    20.045,
    21.844,
    23.217,
    24.445]
    
    L_QFL = 1.2
    
    A_QFS = [0,
    98.95,
    147.71,
    197.23,
    249.64,
    301.81,
    351.91,
    400.77,
    454.51,
    506.28,
    555.25]
    
    T_QFS = [0,
    3.462,
    5.158,
    6.875,
    8.686,
    10.48,
    12.14,
    13.57,
    14.79,
    15.75,
    16.49]
    
    L_QFS = 0.8
    
    A_Q200L = [0.0,
    5.0,
    10.0,
    50.0,
    100.0,
    150.0,
    200.0,
    250.0,
    300.0,
    350.0,
    400.0,
    450.0,
    500.0,
    540.0,
    600.0,
    650.0,
    700.0,
    750.0,
    800.0,
    850.0]
    
    T_Q200L = [0.0,
    0.20,
    0.38,
    1.85,
    3.69,
    5.54,
    7.39,
    9.23,
    11.08,
    12.91,
    14.74,
    16.54,
    18.28,
    19.58,
    21.34,
    22.57,
    23.57,
    24.38,
    25.08,
    25.67]
    
    L_Q200L = 2.0
    
    if magnet_type == "Q74L":
        measured_currents = A_Q74L
        measured_int_gradient = T_Q74L
        length = L_Q74L
        
    if magnet_type == "Q120C":
        measured_currents = A_Q120C
        measured_int_gradient = T_Q120C
        length = L_Q120C
        
    if magnet_type == "QFL":
        measured_currents = A_QFL
        measured_int_gradient = T_QFL
        length = L_QFL
        
    if magnet_type == "QFS":
        measured_currents = A_QFS
        measured_int_gradient = T_QFS
        length = L_QFS
        
    if magnet_type == "Q200L":
        measured_currents = A_Q200L
        measured_int_gradient = T_Q200L
        length = L_Q200L
    
    int_grad = k*length * Brho
    current = np.interp(int_grad, measured_int_gradient, measured_currents)    
    
    return current


# For Dipoles and correctors of the T8 line

def angle(current, magnet_type, Brho):
    
    # Dipoles
    A_MCB = [0,50.13, 100.31, 150.72, 200.86, 250.19, 300.53, 350.88, 401.01, 450.36, 500.18, 550.91, 601.18, 650.55, 700.2, 750.92, 800.7, 850.1, 880.9]
    Bdl_MCB = [0, 0.4201, 0.8379, 1.2594, 1.6678, 2.08807, 2.49581, 2.88241, 3.1638, 3.38286, 3.5695, 3.7365, 3.89024, 4.02763, 4.1436, 4.23895, 4.32285, 4.39996, 4.44424]
    
    # Correctors
    A_CR200 = [0, 50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600]
    Bdl_CR200 = [0, 0.025, 0.050, 0.075, 0.100, 0.125, 0.150, 0.175, 0.199, 0.222, 0.242, 0.260, 0.275]
    
    A_MDXL150 = [0, 10, 20, 40, 50, 80, 100, 120, 150, 160, 180, 200, 220, 240]
    Bdl_MDXL150 = [0, 0.0159, 0.03175, 0.06352, 0.079425, 0.127135, 0.15889, 0.190535, 0.237695, 0.25322, 0.2839, 0.313115, 0.339, 0.361385]
    

    if magnet_type == "MCB":
        measured_currents = A_MCB
        measured_Bdl = Bdl_MCB
    
    if magnet_type == "CR200":
        measured_currents = A_CR200
        measured_Bdl = Bdl_CR200
        
    if magnet_type == "MDXL150":
        measured_currents = A_MDXL150
        measured_Bdl = Bdl_MDXL150
    
    Bdl = np.interp(current, measured_currents, measured_Bdl)
    angle = Bdl / Brho
    return angle # radians

def get_current_dipole(angle, magnet_type, Brho):

    # Dipoles
    A_MCB = [0,50.13, 100.31, 150.72, 200.86, 250.19, 300.53, 350.88, 401.01, 450.36, 500.18, 550.91, 601.18, 650.55, 700.2, 750.92, 800.7, 850.1, 880.9]
    Bdl_MCB = [0, 0.4201, 0.8379, 1.2594, 1.6678, 2.08807, 2.49581, 2.88241, 3.1638, 3.38286, 3.5695, 3.7365, 3.89024, 4.02763, 4.1436, 4.23895, 4.32285, 4.39996, 4.44424]
    
    # Correctors
    A_CR200 = [0, 50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600]
    Bdl_CR200 = [0, 0.025, 0.050, 0.075, 0.100, 0.125, 0.150, 0.175, 0.199, 0.222, 0.242, 0.260, 0.275]
    
    A_MDXL150 = [0, 10, 20, 40, 50, 80, 100, 120, 150, 160, 180, 200, 220, 240]
    Bdl_MDXL150 = [0, 0.0159, 0.03175, 0.06352, 0.079425, 0.127135, 0.15889, 0.190535, 0.237695, 0.25322, 0.2839, 0.313115, 0.339, 0.361385]
    

    if magnet_type == "MCB":
        measured_currents = A_MCB
        measured_Bdl = Bdl_MCB
    
    if magnet_type == "CR200":
        measured_currents = A_CR200
        measured_Bdl = Bdl_CR200
        
    if magnet_type == "MDXL150":
        measured_currents = A_MDXL150
        measured_Bdl = Bdl_MDXL150

    Bdl = angle * Brho  # Compute Bdl from the given angle and Brho
    current = np.interp(Bdl, measured_Bdl, measured_currents)  # Interpolate to find the corresponding current
    return current
from cpymad.madx import Madx
import pandas as pd
import numpy as np
import matplotlib.pyplot as mpl
from matplotlib.pyplot import cm
from pybt.tools.plotters import *
from pybt.tools.parsers import *
from matplotlib.patches import Ellipse, Rectangle, Arc

from scipy.io import loadmat
import pybt
from pybt.tools.parsers import read_twiss_file
from pybt.tools.plotters import *

def draw_aperture_circle(ax, twiss, aper):
  """
  Plot rectangles for apertures that have a type of "circle".

  Parameters:
  - twiss (DataFrame): DataFrame containing the Twiss parameters.
  - aper (str): Column name of the aperture size in the DataFrame.
  - ax (matplotlib.axes._subplots.AxesSubplot): Matplotlib axis to plot on.

  Returns:
  None
  """
  # Iterate through the rows of the twiss DataFrame
  for _, row in twiss[twiss[aper] > 0].iterrows():
    # Check if the aperture type is "circle"
    if row["apertype"] == "circle":
      # Set the facecolor to black
      facecolor = "k"
      # Plot a rectangle for the first half of the aperture
      _ = ax.add_patch(mpl.patches.Rectangle((row['s']-row['l'], 1), row['l'], -1+row[aper], facecolor=facecolor))
      # Plot a rectangle for the second half of the aperture
      _ = ax.add_patch(mpl.patches.Rectangle((row['s']-row['l'], -1), row['l'], 1-row[aper], facecolor=facecolor))

def draw_aperture_rectangle(ax, twiss, aper):
		for _, row in twiss[twiss[aper] > 0].iterrows():
				if row["apertype"]=="rectangle":
						facecolor="k"
						_ = ax.add_patch(mpl.patches.Rectangle((row['s']-row['l'], 1), row['l'], -1+row[aper],facecolor=facecolor))
						_ = ax.add_patch(mpl.patches.Rectangle((row['s']-row['l'], -1), row['l'], 1-row[aper],facecolor=facecolor))
						
def draw_aperture_rectcircle(axH, axV, twiss, aper_1, aper_2, aper_3):
		for _, row in twiss[twiss[aper_1] > 0].iterrows():
				if row["apertype"]=="rectcircle":
						facecolor="k"
						
						_ = axH.add_patch(mpl.patches.Rectangle((row['s']-row['l'], 1), row['l'], -1+row[aper_1],facecolor="gray", alpha=0.5))
						_ = axH.add_patch(mpl.patches.Rectangle((row['s']-row['l'], -1), row['l'], 1-row[aper_1],facecolor="gray", alpha=0.5))
						_ = axH.add_patch(mpl.patches.Rectangle((row['s']-row['l'], 1), row['l'], -1+row[aper_1]+row[aper_3],facecolor=facecolor))
						_ = axH.add_patch(mpl.patches.Rectangle((row['s']-row['l'], -1), row['l'], 1-row[aper_1]-row[aper_3],facecolor=facecolor))
						_ = axV.add_patch(mpl.patches.Rectangle((row['s']-row['l'], 1), row['l'], -1+row[aper_2],facecolor=facecolor))
						_ = axV.add_patch(mpl.patches.Rectangle((row['s']-row['l'], -1), row['l'], 1-row[aper_2],facecolor=facecolor))
						
def draw_aperture_rectcircleH(axH, twiss, aper_1, aper_2, aper_3):
		for _, row in twiss[twiss[aper_1] > 0].iterrows():
				if row["apertype"]=="rectcircle":
						facecolor="k"
						_ = axH.add_patch(mpl.patches.Rectangle((row['s']-row['l'], 1), row['l'], -1+row[aper_1]+row[aper_3],facecolor=facecolor))
						_ = axH.add_patch(mpl.patches.Rectangle((row['s']-row['l'], -1), row['l'], 1-row[aper_1]-row[aper_3],facecolor=facecolor))
						
def draw_aperture_racetrackH(axH, twiss, aper_1, aper_2, aper_3, aper_4):
		for _, row in twiss[twiss[aper_1] > 0].iterrows():
				if row["apertype"]=="racetrack":
						facecolor="k"
						_ = axH.add_patch(mpl.patches.Rectangle((row['s']-row['l'], 1), row['l'], -1+row[aper_1],facecolor="gray", alpha=0.5))
						_ = axH.add_patch(mpl.patches.Rectangle((row['s']-row['l'], -1), row['l'], 1-row[aper_1],facecolor="gray", alpha=0.5))
						_ = axH.add_patch(mpl.patches.Rectangle((row['s']-row['l'], 1), row['l'], -1+row[aper_1]+row[aper_3],facecolor=facecolor))
						_ = axH.add_patch(mpl.patches.Rectangle((row['s']-row['l'], -1), row['l'], 1-row[aper_1]-row[aper_3],facecolor=facecolor))
						
def draw_aperture_racetrackV(axV, twiss, aper_1, aper_2, aper_3, aper_4):
		for _, row in twiss[twiss[aper_1] > 0].iterrows():
				if row["apertype"]=="racetrack":
						facecolor="k"
						_ = axV.add_patch(mpl.patches.Rectangle((row['s']-row['l'], 1), row['l'], -1+row[aper_2],facecolor="gray", alpha=0.5))
						_ = axV.add_patch(mpl.patches.Rectangle((row['s']-row['l'], -1), row['l'], 1-row[aper_2],facecolor="gray", alpha=0.5))
						_ = axV.add_patch(mpl.patches.Rectangle((row['s']-row['l'], 1), row['l'], -1+row[aper_2]+row[aper_3],facecolor=facecolor))
						_ = axV.add_patch(mpl.patches.Rectangle((row['s']-row['l'], -1), row['l'], 1-row[aper_2]-row[aper_3],facecolor=facecolor))
						
def draw_synoptic(ax, twiss):
		ax.set_xlim(0,twiss.s[-1])
		ax.set_ylim(-0.02,1.)
		ax.axis("off")
		fontsize = 16
		offset_s = 0.2
		for _, row in twiss.iterrows():
				label = _
				if (row['keyword'] == 'monitor' and _ != "f61.bctf022" and _ != "f61.bcgaa023"):
						_ = ax.add_patch(mpl.patches.Rectangle( (row['s']-row['l'], 0), row['l'], 0.6, facecolor='r', edgecolor='k'))
						ax.annotate(label,
						xy=(row.s-row.l/2-offset_s, 0.75), xycoords='data',
						xytext=(0.0, 0.0), textcoords='offset points',rotation=90, fontsize=fontsize)
				elif (row['keyword'] == 'marker'):
						if 'inner' in label:
							_ = ax.add_patch(mpl.patches.Rectangle((row['s']-row['l']-0.1, 0), row['l']+0.2, 0.6, facecolor='b', alpha=0.2, edgecolor='k'))
						elif 'hidden' in label:
							_ = ax.add_patch(mpl.patches.Rectangle((row['s']-row['l'], 0), row['l'], 0.1, facecolor='k', alpha=0.5, edgecolor='k'))
						else:
							_ = ax.add_patch(mpl.patches.Rectangle((row['s']-row['l']-0.5, 0), row['l']+1, 0.6, facecolor='b', alpha=0.2, edgecolor='k'))
						# If label does not contain INNER and HIDDEN in the string then add the label
						if (('inner' not in label) and ('hidden' not in label)):
							ax.annotate(label,
							xy=(row.s-row.l/2-offset_s, 0.75), xycoords='data',
							xytext=(0.0, 0.0), textcoords='offset points',rotation=90, fontsize=fontsize*1/2)
				elif (row['keyword'] == 'rbend' or row['keyword'] == 'sbend' or row['keyword'] == 'matrix'):
						_ = ax.add_patch(mpl.patches.Rectangle((row['s']-row['l'], 0), row['l'], 0.6, facecolor='#8DC600', edgecolor='k'))
						ax.annotate(label,
								xy=(row['s']-row['l']/2-offset_s, 0.75), xycoords='data',
								xytext=(0.0, 0.0), textcoords='offset points',rotation=90, fontsize=fontsize)
				elif (row['keyword'] == 'instrument'):
						_ = ax.add_patch(mpl.patches.Rectangle((row['s']-row['l'], 0), row['l'], 0.6, facecolor='magenta', alpha=1.0, edgecolor='k'))
						ax.annotate(label,
								xy=(row['s']-row['l']/2-offset_s, 0.75), xycoords='data',
								xytext=(0.0, 0.0), textcoords='offset points',rotation=90, fontsize=fontsize)
				elif (row['keyword'] == 'quadrupole'):
						_ = ax.add_patch(mpl.patches.Rectangle((row['s']-row['l'], 0), row['l'], 0.6, facecolor='lightblue', alpha=1.0, edgecolor='k'))
						ax.annotate(label,
								xy=(row['s']-row['l']/2-offset_s, 0.75), xycoords='data',
								xytext=(0.0, 0.0), textcoords='offset points',rotation=90, fontsize=fontsize)
				elif (row['keyword'] == 'hkicker' or row['keyword'] == 'vkicker'):
						_ = ax.add_patch(mpl.patches.Rectangle((row['s']-row['l'], 0), row['l'], 0.6, facecolor='blue', alpha=1.0, edgecolor='k'))
						ax.annotate(label,
								xy=(row['s']-row['l']/2-offset_s, 0.75), xycoords='data',
								xytext=(0.0, 0.0), textcoords='offset points',rotation=90, fontsize=fontsize)
				elif (row['keyword'] == 'multipole'):
						_ = ax.add_patch(mpl.patches.Rectangle((row['s']-row['l'], 0), row['l'], 0.6, facecolor='orange', alpha=1.0, edgecolor='orange'))
				if (label == "dumpwall"):
						_ = ax.add_patch(mpl.patches.Rectangle((row['s']-row['l'], 0), row['l'], 0.6, facecolor='blue', alpha=1.0, edgecolor='k'))
						ax.annotate("Dump wall",
								xy=(row['s']-row['l']/2-offset_s, 0.75), xycoords='data',
								xytext=(0.0, 0.0), textcoords='offset points',rotation=90, fontsize=fontsize)
				if (row.name == "t08.bpm073"):
						_ = ax.add_patch(mpl.patches.Rectangle((row['s']-row['l']-0.5, 0), row['l']+0.5, 0.6, facecolor='orange', alpha=1.0, edgecolor='k'))
						ax.annotate("IRRAD BPM1",
								xy=(row['s']-row['l']/2-offset_s, 0.75), xycoords='data',
								xytext=(0.0, 0.0), textcoords='offset points',rotation=90, fontsize=fontsize)
				if (row.name == "t08.bpm080"):
						_ = ax.add_patch(mpl.patches.Rectangle((row['s']-row['l']-0.5, 0), row['l']+0.5, 0.6, facecolor='orange', alpha=1.0, edgecolor='k'))
						ax.annotate("IRRAD BPM2",
								xy=(row['s']-row['l']/2-offset_s, 0.75), xycoords='data',
								xytext=(0.0, 0.0), textcoords='offset points',rotation=90, fontsize=fontsize)
				if (row.name == "t08.bpm085"):
						_ = ax.add_patch(mpl.patches.Rectangle((row['s']-row['l']-0.5, 0), row['l']+0.5, 0.6, facecolor='orange', alpha=1.0, edgecolor='k'))
						ax.annotate("IRRAD BPM3",
								xy=(row['s']-row['l']/2-offset_s, 0.75), xycoords='data',
								xytext=(0.0, 0.0), textcoords='offset points',rotation=90, fontsize=fontsize)
				if (row.name == "t08.bpm092"):
						_ = ax.add_patch(mpl.patches.Rectangle((row['s']-row['l']-0.5, 0), row['l']+0.5, 0.6, facecolor='orange', alpha=1.0, edgecolor='k'))
						ax.annotate("IRRAD BPM4",
								xy=(row['s']-row['l']/2-offset_s, 0.75), xycoords='data',
								xytext=(0.0, 0.0), textcoords='offset points',rotation=90, fontsize=fontsize)
				elif (row['keyword'] == 'octupole'):
						_ = ax.add_patch(mpl.patches.Rectangle((row['s']-row['l'], 0), row['l'], 0.6, facecolor='blueviolet', alpha=1.0, edgecolor='k'))
						ax.annotate(label,
								xy=(row['s']-row['l']/2-offset_s, 0.75), xycoords='data',
								xytext=(0.0, 0.0), textcoords='offset points',rotation=90, fontsize=fontsize)

def plot_graph(twiss, ex, sige, ey, fontsize=16, figsize=(10,4), height_ratios=[1,3,3,3]):
    
    fig, ax = plt.subplots(4,1, figsize=figsize, tight_layout=True, sharex=True, height_ratios=height_ratios)
    
    draw_synoptic(ax[0], twiss)
    
    ax[1].plot(twiss['s'], beam_size(twiss['betx'], twiss['dx'], ex, sige, 1)+twiss.x, alpha=1.0, color ="b", zorder=0)
    ax[1].plot(twiss['s'], -beam_size(twiss['betx'], twiss['dx'], ex, sige, 1)+twiss.x, alpha=1.0, color ="b", zorder=0)
    ax[1].set_xlim(0,twiss.s[-1])
    
    ax[2].plot(twiss['s'], beam_size(twiss['bety'], twiss['dy'], ey, sige, 1)+twiss.y, alpha=1.0, color ="r", zorder=0)
    ax[2].plot(twiss['s'], -beam_size(twiss['bety'], twiss['dy'], ey, sige, 1)+twiss.y, alpha=1.0, color ="r", zorder=0)
    ax[2].set_xlim(0,twiss.s[-1])
    
    ax[1].grid()
    ax[1].set_ylim(-0.12, 0.12)
    ax[1].set_ylabel(r"$\sigma_{H}$ [m]", fontsize=fontsize)
    
    ax[2].grid()
    ax[2].set_ylim(-0.12, 0.12)
    ax[2].set_ylabel(r"$\sigma_{V}$ [m]", fontsize=fontsize)
    
    ax[3].plot(twiss.s, twiss.betx, alpha=1.0, color ="darkblue", zorder=0, label="betx")
    ax[3].plot(twiss.s, twiss.bety, alpha=1.0, color ="darkred", zorder=0, label="bety")
    ax[3].set_xlim(0,twiss.s[-1])
    
    ax[3].legend()
    ax[3].set_ylim(0, 200)
    ax[3].grid()
    ax[3].set_ylabel(r"$\beta$ [m]", fontsize=fontsize)
    
    draw_aperture_circle(ax[1], twiss, "aper_1")
    draw_aperture_circle(ax[2], twiss, "aper_1")
    draw_aperture_rectangle(ax[1], twiss, "aper_1")
    draw_aperture_rectangle(ax[2], twiss, "aper_2")
    draw_aperture_racetrackH(ax[1], twiss, "aper_1", "aper_2", "aper_3", "aper_4")
    draw_aperture_racetrackV(ax[2], twiss, "aper_1", "aper_2", "aper_3", "aper_4")

    plt.show()

def draw_MCB_aperture(ax, half_width, half_height, radius):

	arc = Arc((-half_width,0.0), 2*radius, 2*radius, angle=0.0, theta1=90.0, theta2=270.0, lw=2, color="#8DC600")
	ax.add_patch(arc)

	ax.hlines(half_height, -half_width, half_width, color="#8DC600", lw=2)
	ax.hlines(-half_height, -half_width, half_width, color="#8DC600", lw=2)

	arc = Arc((+half_width,0.0), 2*radius, 2*radius, angle=0.0, theta1=-90.0, theta2=-270.0, lw=2, color="#8DC600")
	ax.add_patch(arc)

def mesh_beam_element(aper_1, aper_3, l, s):

	"""
	This function creates a mesh for a beam element with circular apertures.
	
	Parameters
	----------
	aper_1 : float
			Width of the circular apertures at one end of the beam element.
	aper_3 : float
			Diameter of the circular apertures.
	l : float
			Length of the beam element.
	s : float
			Starting position of the beam element.
			
	Returns
	-------
	x_r, y_r, z_r, i_list, j_list, k_list : numpy arrays
			x_r, y_r, z_r are arrays of the x, y, z positions of the vertices in the mesh.
			i_list, j_list, k_list are arrays of the vertices that make up each triangular face in the mesh.
	"""
	
	r = aper_3

	x=[]
	y=[]

	n = 10
	x0 = 0.0 + aper_1
	y0 = 0.0
	t = np.linspace(0,np.pi/2, 3+n)
	x1 = x0 + r * np.cos(t)
	y1 = y0 + r * np.sin(t)

	x0 = 0.0 - aper_1
	t = np.linspace(np.pi/2,np.pi, 3+n)
	x2 = x0 + r * np.cos(t)
	y2 = y0 + r * np.sin(t)

	x = np.concatenate((x1,x2))
	y = np.concatenate((y1,y2))

	x0 = 0.0 - aper_1
	t = np.linspace(np.pi, 3*np.pi/2, 3+n)
	x3 = x0 + r * np.cos(t)
	y3 = y0 + r * np.sin(t)

	x = np.concatenate((x,x3))
	y = np.concatenate((y,y3))

	x0 = 0.0 + aper_1
	t = np.linspace(3*np.pi/2,2*np.pi, 3+n)
	x4 = x0 + r * np.cos(t)
	y4 = y0 + r * np.sin(t)

	x = np.concatenate((x,x4))
	y = np.concatenate((y,y4))

	x5 = [0.1, -0.1, -0.1, 0.1]
	y5 = [0.1, 0.1, -0.1, -0.1]

	x = np.concatenate((x,x5))
	y = np.concatenate((y,y5))

	x0 = s - l
	
	x_r = np.concatenate((x,x))
	y_r = np.concatenate((y,y))
	z_r = np.concatenate((np.zeros(len(x)),l*np.ones(len(x)))) + x0

	i_list = []
	j_list = []
	k_list = []

	for j in range(4): # For the 4 points		 
		for i in range(int(len(x[0:-4])/4)):
			i_list.append(52+j)
			j_list.append(i+j*int(len(x[0:-4])/4))
			k_list.append(i+1+j*int(len(x[0:-4])/4))

		if j<3:
			i_list.append(52+j)
			j_list.append((j+1)*int(len(x[0:-4])/4))
			k_list.append(52+1+j)

	for j in range(4): # For the 4 points		 
		for i in range(int(len(x[0:-4])/4)):
			i_list.append(len(x)+52+j)
			j_list.append(len(x)+i+j*int(len(x[0:-4])/4))
			k_list.append(len(x)+i+1+j*int(len(x[0:-4])/4))

		if j<3:
			i_list.append(len(x)+52+j)
			j_list.append(len(x)+(j+1)*int(len(x[0:-4])/4))
			k_list.append(len(x)+52+1+j)

	# Faces of the cube
	for i in range(3):
			i_list.append([1*len(x)-4+i])
			j_list.append([2*len(x)-4+i])
			k_list.append([2*len(x)-4+i+1])

	i_list.append(1*len(x)-4+3)
	j_list.append(2*len(x)-4+3)
	k_list.append(2*len(x)-4)

	# Faces of the cube 2
	for i in range(3):
		i_list.append(1*len(x)-4+i)
		j_list.append(1*len(x)-4+i+1)
		k_list.append(2*len(x)-4+i+1)

	i_list.append(1*len(x)-4+3)
	j_list.append(1*len(x)-4+0)
	k_list.append(2*len(x)-4+0)
	
	return x_r, y_r, z_r, i_list, j_list, k_list
  
def plotPSaperture(section = [0], plane = 0, offset = 0):
    '''Plot of the PS aperture based on the Excel aperture data from 2013.
    The keyword section corresponds to the straight sections between which the plot should be limited:
    [0]: whole machine
    [1-100]: plot of the aperture in the desired straight section
    [1,10]: plot of the aperture between the two indicated sections
    plane = 0 for horizontal, plane = 1 for vertical aperture.
    Function returns longitudinal position as well as the aperture for the corresponding plane.'''

    filename = '/eos/user/e/eljohnso/SWAN_projects/east-studies/stripping/Aperture2013_for_Py.mat'
    aperture = loadmat(filename, squeeze_me=True, struct_as_record=False)
    aperture_s = pybt.read_twiss_file('/eos/user/e/eljohnso/SWAN_projects/east-studies/simulations/preliminary/madxchecks/q0_ref.tfs')[1]['s']['pe.seh23']
    
    if plane == 0:
        aperExt = aperture['HorExt']
        aperInt = -aperture['HorInt']
        plt.fill(aperture['s']-aperture_s + offset, aperExt,'grey')
        plt.fill(aperture['s']-aperture_s + offset, aperInt,'grey')
        plt.ylabel('x [mm]') 
        plt.ylim(-200,200)
    elif plane == 1:
        aperExt = aperture['Ver']
        aperInt = -aperture['Ver']
        plt.fill(aperture['s']-aperture_s + offset, aperExt,'grey')
        plt.fill(aperture['s']-aperture_s + offset, aperInt,'grey')
        plt.ylabel('y [mm]')    
        plt.ylim(-100,100)
    if (len(section) == 1) & (section[0] == 0):
        plt.xlim(0,2*np.pi*100)
    elif (len(section) == 1) & (section[0] != 0):
        plt.xlim(aperture['SSlim'][section[0]-1][0],aperture['SSlim'][section[0]-1][1])
    elif (len(section) == 2):
        plt.xlim(aperture['SSlim'][section[0]-1][0],aperture['SSlim'][section[1]-1][1])    
    plt.xlabel('s [m]')  

    return [aperture['s']-aperture_s + offset, aperExt, aperInt]
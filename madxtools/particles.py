import numpy as np

def twiss_parameters_from_distribution(x, xp):
    """
    This function calculates the Twiss parameters (beta, alpha, gamma) and emittance 
    given a distribution of position (x) and angle (xp).

    Args:
        x (numpy.array): A distribution of position
        xp (numpy.array): A distribution of angle

    Returns:
        beta (float): Beta Twiss parameter
        alpha (float): Alpha Twiss parameter
        gamma (float): Gamma Twiss parameter
        epsilon (float): Emittance
    """

    # Calculate averages
    mean_x = np.mean(x)  # average position
    mean_xp = np.mean(xp)  # average angle

    # Calculate second moments
    mean_xx = np.mean(x**2)  # second moment of position
    mean_xxp = np.mean(x*xp)  # cross moment
    mean_xpxp = np.mean(xp**2)  # second moment of angle

    # Calculate geometric emittance
    # This is done using the formula for geometric emittance, which is the square root of
    # the determinant of the second moment matrix of (x, xp)
    epsilon = np.sqrt(mean_xx*mean_xpxp - mean_xxp**2)

    # Calculate Twiss parameters
    beta = mean_xx / epsilon  # Beta is the variance of position divided by emittance
    alpha = -mean_xxp / epsilon  # Alpha is the negative covariance divided by emittance
    gamma = mean_xpxp / epsilon  # Gamma is the variance of angle divided by emittance

    return beta, alpha, gamma, epsilon

import numpy as np
import matplotlib.pyplot as plt
from cpymad.madx import Madx
from pybt.tools.plotters import *
from madxtools.plot_tool import *
from madxtools.math import *
import requests
from madxtools.air_scattering import *
from madxtools.transfer_function import *
from nxcals.api.extraction.data.builders import *
from datetime import datetime

def load_quadrupoles_t8(spark, start, end, selector):

    # Load the B-field and compute the energy
    df = DevicePropertyDataQuery.builder(spark).system("CMW").startTime(start).endTime(end).entity().parameter("PR.BMEAS-B-ST/CycleSamples").build().toPandas()
    df = df.loc[df.selector == selector]
    df['avg_flattop_Gauss'] = df['samples'].apply(lambda x: round(np.mean(x["elements"][1000:1500]), 1)) # Hardcoded flattop time
    df['Ekin'] = df['avg_flattop_Gauss'].apply(lambda x: (193.737692/208)*(np.sqrt(((1/((3.3356/(70.0789*54)*10000)/x))/193.737692)**2+1)-1), 1)
    df["timestamp"] = pd.to_datetime(df['acqStamp'], unit='ns')
    df.sort_values(by="timestamp", inplace=True)
    df = df[df.selector == selector].reset_index(drop=True)
    df['Brho'] = df['Ekin'].apply(lambda x: pb_ion_rigidity(x))
    df_energy = df[['timestamp', 'selector', 'avg_flattop_Gauss', 'Ekin', 'Brho']]
    df_energy.sort_values(by="timestamp", inplace=True)
    df_energy.head(3)

    # Load the quadrupoles current and merge to energy
    df_quadrupoles = pd.DataFrame()
    magnet_names = ["F61.QFN007/MEAS.PULSE",
                   "F61.QDN014/MEAS.PULSE",
                   "F61.QFN021/MEAS.PULSE",
                   "F61.QDN030/MEAS.PULSE",
                   "T08.QFN021/MEAS.PULSE",
                   "T08.QDN023/MEAS.PULSE",
                   "T08.QDN061/MEAS.PULSE",
                   "T08.QFN066/MEAS.PULSE"]

    for magnet_name in magnet_names:
        df = DevicePropertyDataQuery.builder(spark).system("CMW").startTime(start).endTime(end).entity().parameter(magnet_name).build().toPandas()
        df = df[df.selector == selector].reset_index(drop=True)
        df = df[['VALUE', 'acqStamp', 'selector']]
        df['magnet_name'] = magnet_name
        df["timestamp"] = pd.to_datetime(df['acqStamp'], unit='ns')
        df.sort_values(by="timestamp", inplace=True)
        df_energy.sort_values(by="timestamp", inplace=True)
        merged_df = pd.merge_asof(df_energy, df, on='timestamp', by='selector', direction='nearest')
        df_quadrupoles = pd.concat([df_quadrupoles, merged_df], ignore_index=True)

    df_quadrupoles.reset_index(drop=True, inplace=True)
    df_quadrupoles.sort_values(by="timestamp", inplace=True)

    df_quadrupoles.drop(columns=['acqStamp'], inplace=True)
    df_quadrupoles.rename(columns={'VALUE': 'current'}, inplace=True)
    df_quadrupoles.reset_index(inplace=True, drop = True)

    # Convert to k1
    keys = ["F61.QFN007/MEAS.PULSE",
           "F61.QDN014/MEAS.PULSE",
           "F61.QFN021/MEAS.PULSE",
           "F61.QDN030/MEAS.PULSE",
           "T08.QFN021/MEAS.PULSE",
           "T08.QDN023/MEAS.PULSE",
           "T08.QDN061/MEAS.PULSE",
           "T08.QFN066/MEAS.PULSE"]

    values = ["Q74L",
              "Q120C",
              "QFL",
              "QFS",
              "QFL",
              "QFL",
              "Q200L",
              "Q200L"]

    mapping = dict(zip(keys, values))
    df_quadrupoles["type"] = df_quadrupoles['magnet_name'].map(mapping)
    df_quadrupoles['k1'] = df_quadrupoles.apply(lambda row: k1(row['current'], row.type, Brho=row['Brho']) if row.magnet_name == "F61.QFN007/MEAS.PULSE" else (82/54)*k1(row['current'], row.type, row["Brho"]), axis=1)
    
    return df_quadrupoles
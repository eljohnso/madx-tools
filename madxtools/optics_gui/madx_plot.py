import tkinter as tk
from madxtools.nxcals import *
from nxcals.spark_session_builder import get_or_create, Flavor
from nxcals import spark_session_builder
import threading
import pickle
import optics_gui_functions as ogf

def plot_madx(canvas, fig, ax, year_entry, month_entry, day_entry, hour_entry, minute_entry, second_entry):

    for single_ax in fig.axes:
        single_ax.clear()

    ts = ogf.get_timestamp(year_entry, month_entry, day_entry, hour_entry, minute_entry, second_entry)

    print(ts)

    # target_timestamp = pd.Timestamp('2023-10-25 14:10:00.000')
    target_timestamp = ts

    with open('df_quadrupoles.pickle', 'rb') as f:
        df_quadrupoles = pickle.load(f)

    Ekin, optics_nxcals = ogf.get_optics_and_ekin(df_quadrupoles, target_timestamp)
    optics_nxcals_str = np.array_str(np.array(optics_nxcals), precision=4)
    print(f"{Ekin:.2f} GeV", optics_nxcals_str)


    p, beta, gamma = pb_ion_p_beta_gamma(np.mean(Ekin))
    charge = 54
    print(f"p = {p/54:.2f} GeV/c")

    # Matched initial parameters 13nd oct 2023
    betx0 = 53.074
    alfx0 = -13.191
    dx0 = 0.13
    dpx0 = 0.02
    exn = 2.53e-5

    bety0 = 3.675
    alfy0 = 0.859
    dy0 = 0.0
    dpy0 = 0.0
    eyn = 6.94e-06

    sige = 0.0045

    ex = exn/(beta*gamma)
    ex_initial = ex
    ey = eyn/(beta*gamma)
    ey_initial = ey

    with open('tempfile', 'w') as f:
        madx = Madx(stdout=f,stderr=f)
        madx.option(verbose=True, debug=False, echo=True, warn=True, twiss_print=False)
    
    madx.input(requests.get("https://gitlab.cern.ch/eljohnso/acc-models-tls-eliott-fork/-/raw/EliottBranch/ps_extraction/f61t8/f61t8_op.str").text)
    madx.input(requests.get("https://gitlab.cern.ch/eljohnso/acc-models-tls-eliott-fork/-/raw/EliottBranch/ps_extraction/f61t8/f61t8_op.seq").text)

    # Optics
    optics = optics_nxcals
    madx.input("kQFN1 = "+str(optics[0])+";")
    madx.input("kQDN2 = "+str(-optics[1])+";")
    madx.input("kQFN3 = "+str(optics[2])+";")
    madx.input("kQDN4 = "+str(-optics[3])+";")
    madx.input("kQFN5 = "+str(optics[4])+";")
    madx.input("kQDN6 = "+str(-optics[5])+";")
    madx.input("kQDN7 = "+str(-optics[6])+";")
    madx.input("kQFN8 = "+str(optics[7])+";")

    # Add the Air region
    sequence = "f61t8_op"

    p1 = madx.sequence['f61t8_op'].elements["f61.btv012"].position - 0.5 # This dimension needs to be checked
    p2 = madx.sequence['f61t8_op'].elements["f61.btv012"].position
    add_air_region(madx, "1", sequence, p1, p2, (p2-p1)/2)

    p1 = madx.sequence['f61t8_op'].elements["f61.bctf022"].position
    p2 = madx.sequence['f61t8_op'].elements["f61.mbxhd025"].position + madx.sequence['f61t8_op'].elements["f61.mbxhd025"].length + 0.3
    add_air_region(madx, "2", sequence, p1, p2, p2-p1-0.3)

    #t08.tbs068, t08.xsec070, t08.xion071, t08.bctf072, t08.bpm073
    p1 = madx.sequence['f61t8_op'].elements["t08.tbs068"].position + madx.sequence['f61t8_op'].elements["t08.tbs068"].length 
    p2 = madx.sequence['f61t8_op'].elements["t08.xsec070"].position
    add_air_region(madx, "3", sequence, p1, p2, (p2-p1)/2)

    p1 = madx.sequence['f61t8_op'].elements["t08.xsec070"].position + madx.sequence['f61t8_op'].elements["t08.xsec070"].length 
    p2 = madx.sequence['f61t8_op'].elements["t08.xion071"].position
    add_air_region(madx, "4", sequence, p1, p2, (p2-p1)/2)

    p1 = madx.sequence['f61t8_op'].elements["t08.xion071"].position + madx.sequence['f61t8_op'].elements["t08.xion071"].length 
    p2 = madx.sequence['f61t8_op'].elements["t08.bctf072"].position
    add_air_region(madx, "5", sequence, p1, p2, (p2-p1)/2)

    p1 = madx.sequence['f61t8_op'].elements["t08.bctf072"].position + madx.sequence['f61t8_op'].elements["t08.bctf072"].length 
    p2 = madx.sequence['f61t8_op'].elements["t08.bpm073"].position
    add_air_region(madx, "6", sequence, p1, p2, (p2-p1)/2)

    pos_t08_vac_chamber75 = madx.sequence['f61t8_op'].elements["T08.VACCUM_CHAMBER75"].position
    len_t08_vac_chamber75 = madx.sequence['f61t8_op'].elements["T08.VACCUM_CHAMBER75"].length
    pos_t08_bpm092 = madx.sequence['f61t8_op'].elements["T08.BPM092"].position
    add_air_region(madx, "7", sequence, pos_t08_vac_chamber75 + len_t08_vac_chamber75, pos_t08_bpm092, 1)

    pos_t08_xion094 = madx.sequence['f61t8_op'].elements["T08.xion094"].position
    len_t08_xion094 = madx.sequence['f61t8_op'].elements["T08.xion094"].length
    pos_t08_mwpc = madx.sequence['f61t8_op'].elements["T08.xwcm103"].position
    add_air_region(madx, "8", sequence, pos_t08_xion094 + len_t08_xion094, pos_t08_mwpc, 1)


    # add_hidden_marker(madx, sequence, 20, 100, steps)

    madx.command.beam(particle='PROTON',pc=p/charge,exn=exn,eyn=eyn)
    madx.input('BRHO      := BEAM->PC * 3.3356;')
    madx.input('SELECT, FLAG=TWISS, COLUMN=NAME,KEYWORD,S,L,K1L,BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APERTYPE,APER_1,APER_2,APER_3,APER_4,KMIN,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;')
    madx.use(sequence=sequence)

    # make thin
    madx.use(sequence="f61t8_op")
    madx.input('SEQEDIT, sequence=f61t8_op;')
    madx.input('FLATTEN;')
    madx.input('ENDEDIT;')
    madx.use(sequence="f61t8_op")
    madx.input("SELECT, FLAG=makethin, CLASS=dipole, SLICE=3;")
    # madx.input("MAKETHIN, SEQUENCE=f61t8_op, style=TEAPOT")
    madx.use(sequence="f61t8_op")


    twiss = madx.twiss(betx=betx0, bety=bety0, alfx=alfx0, alfy=alfy0, Dx=dx0, Dy=dy0, Dpx=dpx0, Dpy=dpy0).dframe()

    # plot_graph(twiss, ex, sige, ey, fontsize=12, figsize=(15,8), height_ratios=[1,3,3,1])

    print("Calculating beam size...")

    # Air scattering
    madx.use(sequence=sequence)
    madx.input(f"SAVEBETA, LABEL=savebeta_air, PlACE = AIR_START, SEQUENCE={sequence};")
    twiss = madx.twiss(betx=betx0, bety=bety0, alfx=alfx0, alfy=alfy0, Dx=dx0, Dy=dy0, Dpx=dpx0, Dpy=dpy0).dframe()
    twiss, twiss_scattered = process_scattering(madx, exn, eyn, beta, gamma, sequence, "t08.end", p/charge, twiss, ex_initial, ey_initial)

    fontsize=16
    figsize=(15,12)
    height_ratios=[1,3,3,1]

    draw_synoptic(ax[0], twiss_scattered)

    ax[1].plot(twiss_scattered['s'], beam_size(twiss_scattered['betx'], twiss_scattered['dx'], twiss_scattered['ex'], sige, 1)+twiss_scattered.x, alpha=1.0, color ="b", zorder=0)
    ax[1].plot(twiss_scattered['s'], -beam_size(twiss_scattered['betx'], twiss_scattered['dx'], twiss_scattered['ex'], sige, 1)+twiss_scattered.x, alpha=1.0, color ="b", zorder=0)
    ax[1].set_xlim(0,twiss_scattered.s[-1])

    ax[2].plot(twiss_scattered['s'], beam_size(twiss_scattered['bety'], twiss_scattered['dy'], twiss_scattered['ey'], sige, 1)+twiss_scattered.y, alpha=1.0, color ="r", zorder=0)
    ax[2].plot(twiss_scattered['s'], -beam_size(twiss_scattered['bety'], twiss_scattered['dy'], twiss_scattered['ey'], sige, 1)+twiss_scattered.y, alpha=1.0, color ="r", zorder=0)
    ax[2].set_xlim(0,twiss_scattered.s[-1])

    ax[1].grid()
    ax[1].set_ylim(-0.12, 0.12)
    ax[1].set_ylabel(r"$\sigma_{H}$ [m]", fontsize=fontsize)

    ax[2].grid()
    ax[2].set_ylim(-0.12, 0.12)
    ax[2].set_ylabel(r"$\sigma_{V}$ [m]", fontsize=fontsize)

    ax[3].plot(twiss_scattered.s, twiss_scattered.betx, alpha=1.0, color ="darkblue", zorder=0, label="betx")
    ax[3].plot(twiss_scattered.s, twiss_scattered.bety, alpha=1.0, color ="darkred", zorder=0, label="bety")
    ax[3].set_xlim(0,twiss_scattered.s[-1])

    ax[3].legend()
    ax[3].set_ylim(0, 200)
    ax[3].grid()
    ax[3].set_ylabel(r"$\beta$ [m]", fontsize=fontsize)

    draw_aperture_circle(ax[1], twiss_scattered, "aper_1")
    draw_aperture_circle(ax[2], twiss_scattered, "aper_1")
    draw_aperture_rectangle(ax[1], twiss_scattered, "aper_1")
    draw_aperture_rectangle(ax[2], twiss_scattered, "aper_2")
    draw_aperture_racetrackH(ax[1], twiss_scattered, "aper_1", "aper_2", "aper_3", "aper_4")
    draw_aperture_racetrackV(ax[2], twiss_scattered, "aper_1", "aper_2", "aper_3", "aper_4")


    # Beam size at OCtavius
    madx_octavius_H = beam_size(twiss_scattered.loc["t08.octavius"]['betx'], twiss_scattered.loc["t08.octavius"]['dx'], twiss_scattered.loc["t08.octavius"]['ex'], sige, 1)
    madx_octavius_V =  beam_size(twiss_scattered.loc["t08.octavius"]['bety'], twiss_scattered.loc["t08.octavius"]['dy'], twiss_scattered.loc["t08.octavius"]['ey'], sige, 1)

    # Box with initial parameters
    textstr = '\n'.join((
        "Matched initial parameters:",
        r'$p$='+str(round(p/charge,3)),
        r'$\beta_{x}$='+str(round(betx0,3))+r', $\alpha_{x}$='+str(round(alfx0,3)),
        r'$\beta_{y}$='+str(round(bety0,3))+r', $\alpha_{y}$='+str(round(alfy0,3)),
        r'$D_{x}$='+str(round(dx0,3))+r', $D_{y}$='+str(round(dy0,3)),
        r'$D_{px}$='+str(round(dpx0,3))+r', $D_{py}$='+str(round(dpy0,3)),
        r'$exn$='+str(round(exn,8))+r', $eyn$='+str(round(eyn,8)),
        r'$\frac{dp}{p}$='+str(round(sige,6)),
        ))

    props = dict(boxstyle='square', facecolor='white', alpha=0.9)

    # place a text box in upper left in axes coords
    ax[1].text(0., 0.95, textstr, transform=ax[1].transAxes, fontsize=10,
            verticalalignment='top', bbox=props)


    # Box with optics
    textstr = '\n'.join((
        "Optics:",
        "kQFN1 = "+str(round(optics[0],3)),
        "kQDN2 = "+str(round(-optics[1],3)),
        "kQFN3 = "+str(round(optics[2],3)),
        "kQDN4 = "+str(round(-optics[3],3)),
        "kQFN5 = "+str(round(optics[4],3)),
        "kQDN6 = "+str(round(-optics[5],3)),
        "kQDN7 = "+str(round(-optics[6],3)),
        "kQFN8 = "+str(round(optics[7],3)),
        "Octavius H = "+str(round(madx_octavius_H*1000,1))+" mm",
        "Octavius V = "+str(round(madx_octavius_V*1000,1))+" mm",
        ))

    props = dict(boxstyle='square', facecolor='white', alpha=0.9)

    # place a text box in upper left in axes coords
    ax[1].text(0.7, 0.95, textstr, transform=ax[1].transAxes, fontsize=10,
            verticalalignment='top', bbox=props)
    # fig.suptitle(f"{data[data.selector == 'CPS.USER.MD3'].timestamp.iloc[loc].strftime('%H:%M %d/%m/%y')}", fontsize=20)

    canvas.draw()
    print("OK")
    
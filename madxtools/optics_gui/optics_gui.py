import tkinter as tk
from tkinter import ttk
import optics_gui_functions as ogf
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
import pandas as pd
import pickle
import numpy as np
from madxtools.math import *
from madxtools.air_scattering import *
from cpymad.madx import Madx
import requests
import matplotlib.colors as colors
from datetime import datetime

def my_mpl_style():
    '''Sets my preferred style options for matplotlib.'''
    import matplotlib as mpl
    # Font sizes
    scaling_factor = 1
    mpl.rcParams['axes.labelsize'] = 18/scaling_factor
    mpl.rcParams['legend.fontsize'] = 14/scaling_factor
    mpl.rcParams['axes.titlesize'] = 20/scaling_factor
    mpl.rcParams['xtick.labelsize'] = 16/scaling_factor
    mpl.rcParams['ytick.labelsize'] = 16/scaling_factor
    mpl.rcParams['axes.formatter.limits'] = (-2, 3)
    # mpl.rcParams['font.size'] = 10
    mpl.rcParams['figure.dpi'] = 50
my_mpl_style()

global spark
spark = None

df_quadrupoles = None

def main():
    global spark
    global df_quadrupoles
    root = tk.Tk()
    root.title("Optics GUI")

    notebook = ttk.Notebook(root)
    notebook.pack(fill='both', expand=True)

    # Create tabs
    tab1 = ttk.Frame(notebook)
    notebook.add(tab1, text='NXCALS')

    tab2 = ttk.Frame(notebook)
    notebook.add(tab2, text='MAD-X')

    # Get the screen width and height
    screen_width = root.winfo_screenwidth()
    screen_height = root.winfo_screenheight()

    # Calculate the x and y coordinates for the window to be centered
    window_width = 1200
    window_height = 1000
    x = int((screen_width / 2) - (window_width / 2))
    y = int((screen_height / 2) - (window_height / 2))

    # Set the window size and position
    root.geometry(f"{window_width}x{window_height}+{x}+{y}")

    label = tk.Label(tab1, text="Welcome to tkinter!")
    label.pack()

    status_label = tk.Label(tab1, text="Spark Status: Not Created", bg="red", width=20)
    status_label.pack()


    # Radio button
    selected_label = tk.Label(tab1, text="Spark creation mode:")
    selected_label.pack()
    selected_spark_option = tk.StringVar(value="local")
    tk.Radiobutton(tab1, text="Local", variable=selected_spark_option, value="local").pack()
    tk.Radiobutton(tab1, text="YARN", variable=selected_spark_option, value="yarn").pack()
    
    

    text_box = tk.Text(tab1, height=10, width=50)


    button_spark = tk.Button(tab1, text="Create Spark", command=lambda: ogf.create_spark(label, status_label, selected_spark_option))
    button_spark.pack()

    button_load_nxcals = tk.Button(tab1, text="Load nxcals data", command=lambda: ogf.load_nxcals(df_label, text_box))
    button_load_nxcals.pack()

    df_label = tk.Label(tab1, text="Df Status: Not Created", bg="red", width=20)
    df_label.pack()

    
    text_box.pack()



    timestamp_label = tk.Label(tab1, text="Enter timestamp (YYYY MM DD HH MM SS):")
    timestamp_label.pack()

    specific_timestamp = datetime(2023, 10, 25, 14, 10, 0)

    year_entry = tk.Entry(tab1, width=5)
    year_entry.insert(0, specific_timestamp.year)

    month_entry = tk.Entry(tab1, width=3)
    month_entry.insert(0, specific_timestamp.month)

    day_entry = tk.Entry(tab1, width=3)
    day_entry.insert(0, specific_timestamp.day)

    hour_entry = tk.Entry(tab1, width=3)
    hour_entry.insert(0, specific_timestamp.hour)

    minute_entry = tk.Entry(tab1, width=3)
    minute_entry.insert(0, specific_timestamp.minute)

    second_entry = tk.Entry(tab1, width=3)
    second_entry.insert(0, specific_timestamp.second)

    year_entry.pack()
    month_entry.pack()
    day_entry.pack()
    hour_entry.pack()
    minute_entry.pack()
    second_entry.pack()

    # Button to fetch the timestamp
    timestamp_button = tk.Button(tab1, text="Fetch Timestamp", command= lambda:ogf.get_timestamp(year_entry, month_entry, day_entry, hour_entry, minute_entry, second_entry))
    timestamp_button.pack()


    fig, ax = plt.subplots(figsize=(15, 6))

    # Create a canvas to display the figure
    canvas = FigureCanvasTkAgg(fig, master=tab1)
    canvas_widget = canvas.get_tk_widget()

    refresh_button = tk.Button(tab1, text="Refresh Data", command=lambda: ogf.update_plot(canvas, fig, ax, df_quadrupoles))
    refresh_button.pack()

    canvas_widget.pack()

    ### SECOND TAB MAD-X
    
    fontsize=16
    figsize=(15,12)
    height_ratios=[1,3,3,1]
    fig_madx, ax_madx = plt.subplots(4,1, figsize=figsize, tight_layout=True, sharex=True, height_ratios=height_ratios)
      
    canvas_madx = FigureCanvasTkAgg(fig_madx, master=tab2)
    canvas_madx_widget = canvas_madx.get_tk_widget()

    madx_button = tk.Button(tab2, text="MAD Calculate", command=lambda: ogf.plot_madx(canvas_madx, fig_madx, ax_madx, year_entry, month_entry, day_entry, hour_entry, minute_entry, second_entry))
    madx_button.pack()

    canvas_madx_widget.pack()


    root.mainloop()

if __name__ == "__main__":
    main()

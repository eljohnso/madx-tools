import tkinter as tk
from tkinter import ttk
import optics_gui_functions as ogf
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
import pandas as pd
import pickle
import numpy as np
from madxtools.math import *
from madxtools.air_scattering import *
from cpymad.madx import Madx
import requests
import matplotlib.colors as colors
from datetime import datetime

def my_mpl_style():
    '''Sets my preferred style options for matplotlib.'''
    import matplotlib as mpl
    # Font sizes
    scaling_factor = 1
    mpl.rcParams['axes.labelsize'] = 18/scaling_factor
    mpl.rcParams['legend.fontsize'] = 14/scaling_factor
    mpl.rcParams['axes.titlesize'] = 20/scaling_factor
    mpl.rcParams['xtick.labelsize'] = 16/scaling_factor
    mpl.rcParams['ytick.labelsize'] = 16/scaling_factor
    mpl.rcParams['axes.formatter.limits'] = (-2, 3)
    # mpl.rcParams['font.size'] = 10
    mpl.rcParams['figure.dpi'] = 50
my_mpl_style()

global spark
spark = None

df_quadrupoles = None

def main():
    global spark
    global df_quadrupoles
    root = tk.Tk()
    root.title("Optics GUI")


    # Get the screen width and height
    screen_width = root.winfo_screenwidth()
    screen_height = root.winfo_screenheight()

    # Calculate the x and y coordinates for the window to be centered
    window_width = 800
    window_height = 400
    x = int((screen_width / 2) - (window_width / 2))
    y = int((screen_height / 2) - (window_height / 2))

    # Set the window size and position
    root.geometry(f"{window_width}x{window_height}+{x}+{y}")

    label = tk.Label(root, text="Welcome to tkinter!")
    label.pack()

    status_label = tk.Label(root, text="Spark Status: Not Created", bg="red", width=20)
    status_label.pack()


    # Radio button
    selected_label = tk.Label(root, text="Spark creation mode:")
    selected_label.pack()
    selected_spark_option = tk.StringVar(value="local")
    tk.Radiobutton(root, text="Local", variable=selected_spark_option, value="local").pack()
    tk.Radiobutton(root, text="YARN", variable=selected_spark_option, value="yarn").pack()
    

    button_spark = tk.Button(root, text="Create Spark", command=lambda: ogf.create_spark(label, status_label, selected_spark_option))
    button_spark.pack()

    button_load_nxcals = tk.Button(root, text="Load nxcals data", command=lambda: ogf.load_nxcals(df_label, start_entry, end_entry))
    button_load_nxcals.pack()

    df_label = tk.Label(root, text="Df Status: Not Created", bg="red", width=20)
    df_label.pack()

    timestamp_label = tk.Label(root, text="NXCALS retrieval range:")
    timestamp_label.pack()

    start = "2023-10-24 08:00:00.000"
    end = "2023-10-24 08:01:00.000"

    start_entry = tk.Entry(root, width=20)
    start_entry.insert(0, start)

    end_entry = tk.Entry(root, width=20)
    end_entry.insert(0, end)

    start_entry.pack()
    end_entry.pack()


    root.mainloop()

if __name__ == "__main__":
    main()

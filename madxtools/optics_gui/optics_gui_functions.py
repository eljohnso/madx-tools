import tkinter as tk
from madxtools.nxcals import *
from nxcals.spark_session_builder import get_or_create, Flavor
from nxcals import spark_session_builder
import threading
import pickle
from madx_plot import *
import time

def create_spark(label, status_label, selected_spark_option):
    def task():
        global spark
        print("Starting Spark creation task...")  # Debugging print
        status_label.config(bg="orange")
        status_label.config(text="Spark Status: Creating")

        print("Creating spark...")

        # look at the value of spark_creation_mode
        selected_spark_option_value = selected_spark_option.get()
        print("Selected spark option:", selected_spark_option_value)

        if selected_spark_option_value == "local":
            status_label.config(text="Spark Status: Creating in local mode")
            spark = SparkSession.builder \
                .appName("GUI_LOCAL") \
                .config("spark.driver.memory", "4g") \
                .config("spark.executor.memory", "4g") \
                .getOrCreate()
            label.config(text="Spark object created in local mode!")
        elif selected_spark_option_value == "yarn":
            status_label.config(text="Spark Status: Creating in YARN mode")
            spark = get_or_create(app_name='GUI_YARN_LARGE', flavor=Flavor.YARN_LARGE)
            label.config(text="Spark object created in YARN mode!")

        if spark is not None:
            print("Spark object created:", spark)
            status_label.config(bg="green")
            status_label.config(text="Spark Status: Created")
        else:
            status_label.config(bg="red")

    thread = threading.Thread(target=task)
    thread.start()

def load_nxcals(df_label, start_entry, end_entry): 
    def task():
        global spark
        global df_quadrupoles
        if spark is not None:
            df_label.config(bg="orange")
            df_label.config(text="Df Status: Loading")
            # Use the spark object here
            print("Using spark:", spark)
            start = start_entry.get()
            end = end_entry.get()
            print("Start:", start)
            print("End:", end)
            selector = "CPS.USER.MD3"
            df_quadrupoles = load_quadrupoles_t8(spark, start, end, selector)
            print(df_quadrupoles.head(3))
            df_label.config(bg="green")
            df_label.config(text="Df Status: Loaded")
            with open('df_quadrupoles_gui.pickle', 'wb') as handle:
                pickle.dump(df_quadrupoles, handle, protocol=pickle.HIGHEST_PROTOCOL)
        else:
            print("Spark object is not created yet.")
    thread = threading.Thread(target=task)
    thread.start()



def update_plot(canvas, fig, ax, df_quadrupoles):
    ax.clear()

    with open('df_quadrupoles_gui.pickle', 'rb') as f:
        df_quadrupoles = pickle.load(f)

    for magnet_name in df_quadrupoles.magnet_name.unique():
        df = df_quadrupoles[df_quadrupoles.magnet_name == magnet_name]
        ax.plot(df.timestamp, df.k1, label=magnet_name)

    ax.set_ylabel('k1')
    ax.set_xlabel('Time')

    ax.legend()

    fig.suptitle('Large optics')

    canvas.draw()


def get_optics_and_ekin(df_quadrupoles, approx_timestamp):
    approx_timestamp = pd.to_datetime(approx_timestamp)

    optics_nxcals = []
    Ekin_list = []

    magnet_names = ["F61.QFN01/MEAS.PULSE",
            "F61.QDN02/MEAS.PULSE",
            "F61.QFN03/MEAS.PULSE",
            "F61.QDN04/MEAS.PULSE",
            "T8.QFN05/MEAS.PULSE",
            "T8.QDN06/MEAS.PULSE",
            "T8.QDN07/MEAS.PULSE",
            "T8.QFN08/MEAS.PULSE"]

    for magnet_name in magnet_names:
        # print(magnet_name)
        df = df_quadrupoles[df_quadrupoles.magnet_name == magnet_name]

        index_closest_to_approx_timestamp = (pd.to_datetime(df.timestamp) - approx_timestamp).abs().idxmin()

        # print(df.loc[index_closest_to_approx_timestamp].timestamp)

        optics_nxcals.append(df.loc[index_closest_to_approx_timestamp].k1)
        Ekin_list.append(df.loc[index_closest_to_approx_timestamp].Ekin)

    mean_Ekin = np.mean(Ekin_list)
    # print(f"{mean_Ekin:.2f} GeV")
    # print(optics_nxcals)

    return mean_Ekin, optics_nxcals


from datetime import datetime

def get_timestamp(year_entry, month_entry, day_entry, hour_entry, minute_entry, second_entry):
    print("hello")
    try:
        ts = datetime(int(year_entry.get()), int(month_entry.get()), int(day_entry.get()), 
                      int(hour_entry.get()), int(minute_entry.get()), int(second_entry.get()))
        print("Timestamp selected:", ts)
        return ts
    except ValueError as e:
        print("Invalid timestamp:", e)
        return None

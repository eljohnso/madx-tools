import tkinter as tk
from tkinter import DoubleVar
from matplotlib.figure import Figure
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import numpy as np
from pybt.tools.plotters import *

def twiss_distribution(beta, alpha, emittance, num_particles):
    # Calculate sigma from twiss parameters
    sigma = np.sqrt(emittance * beta)
    # Define mean and covariance
    mean = [0, 0]
    covariance = emittance * np.array([[beta, -alpha], [-alpha, (alpha**2 + 1)/beta]])
    # Generate distribution
    return np.random.multivariate_normal(mean, covariance, num_particles)

def update_distribution(event=None):
    # Get values from sliders
    beta = beta_var.get()
    alpha = alpha_var.get()
    emittance = emittance_var.get()
    num_particles = num_particles_var.get()
    alpha_plot = alpha_plot_var.get()
    # Generate distribution
    distribution = twiss_distribution(beta, alpha, emittance, int(num_particles))
    # Update plot
    ax.clear()
    ax.scatter(distribution[:,0], distribution[:,1], s=1, alpha=alpha_plot)
    ax.set_xlim([-0.1, 0.1])
    ax.set_ylim([-0.1, 0.1])
    ax.set_xlabel("x (m)")
    ax.set_ylabel("x' (rad)")
    ax.set_title("Particle Distribution in Phase Space")


    draw_ellipse(alpha, beta, emittance, ax=ax)

    fig.canvas.draw()


# Create a window
root = tk.Tk()

# Add sliders
beta_var = DoubleVar()
beta_slider = tk.Scale(root, from_=1, to=200, resolution=0.01, length=400, orient="horizontal", variable=beta_var, label="Beta")
beta_slider.pack()

alpha_var = DoubleVar()
alpha_slider = tk.Scale(root, from_=-30, to=30, resolution=0.1, length=400, orient="horizontal", variable=alpha_var, label="Alpha")
alpha_slider.pack()

emittance_var = DoubleVar()
emittance_slider = tk.Scale(root, from_=0, to=1e-5, resolution=1e-6, length=400, orient="horizontal", variable=emittance_var, label="Emittance")
emittance_slider.pack()

num_particles_var = DoubleVar()
num_particles_slider = tk.Scale(root, from_=100, to=10000, resolution=10, length=400, orient="horizontal", variable=num_particles_var, label="Number of Particles")
num_particles_slider.pack()

alpha_plot_var = DoubleVar()
alpha_plot_slider = tk.Scale(root, from_=0, to=0.25, resolution=0.01, length=400, orient="horizontal", variable=alpha_plot_var, label="Transparancy (plot)")
alpha_plot_slider.pack()

# Bind sliders to update function
beta_slider.bind("<ButtonRelease-1>", update_distribution)
alpha_slider.bind("<ButtonRelease-1>", update_distribution)
emittance_slider.bind("<ButtonRelease-1>", update_distribution)
num_particles_slider.bind("<ButtonRelease-1>", update_distribution)
alpha_plot_slider.bind("<ButtonRelease-1>", update_distribution)

# Create a figure
fig = Figure(figsize=(6,6), tight_layout=True)
ax = fig.add_subplot(111)

# Add figure to tkinter using a canvas
canvas = FigureCanvasTkAgg(fig, master=root)
canvas.draw()
canvas.get_tk_widget().pack()

# Set starting values for the sliders
beta_var.set(5.0)
alpha_var.set(-5.0)
emittance_var.set(5e-6)
num_particles_var.set(5000)
alpha_plot_var.set(0.25)
update_distribution()

root.mainloop()
